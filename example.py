import glfw
import OpenGL.GL as gl

# Rendering functions

def background(self, r=0, g=0, b=0, a=0):
    gl.glClearColor(r, g, b, a)
    gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)

def rect(x1, y1, w, h):
    x2 = x1 + w
    y2 = y1 + h
    gl.glBegin(gl.GL_QUADS)
    gl.glVertex2f(x1, y1)
    gl.glVertex2f(x2, y1)
    gl.glVertex2f(x2, y2)
    gl.glVertex2f(x1, y2)
    gl.glEnd()
 
def color(r, g, b, a=1):
    gl.glColor(r, g, b, a)

# Draw stuff here...
 
t = 0

def draw():
    global t
    background(0.2, 0.2, 0.3)
    rect(t, 0.5, 0.1, 0.1)
    t += 0.01
    if t > 1.0:
        t = -1.0

def main():
    if not glfw.init():
        return
    window = glfw.create_window(640, 480, "Example", None, None)
    if not window:
        glfw.terminate()
        return
    glfw.make_context_current(window)

    while not glfw.window_should_close(window):
        draw()
        glfw.swap_buffers(window)
        glfw.poll_events()

    glfw.terminate()

if __name__ == "__main__":
    main()

