# A minimal-ish glfw/opengl example.

## Setup virtual environment

```
cd glfwexample 
python3 -m venv ve3
source ve3/bin/activate
pip install glfw pyopengl
```

## Deactivate virtual environment

```
deactivate
```

## Reactivate + run example

```
source ve3/bin/activate
python example.py
```
